import java.util.ArrayList;
import java.util.List;

public class Principal {

    public static void main(String[] args) {
        /* animal1 = new Animal();
        animal1.comer();
        animal1.dormir();
        animal1.hacerRuido();*/

       /* Perro perro = new Perro();
        perro.comer();
        perro.dormir();
        perro.hacerRuido();
        perro.moverCola();

        Humano humano = new Humano("José");
        humano.comer();
        humano.dormir();
        humano.hacerRuido();
        humano.trabajar();
        humano.volar();

        // a = new Humano("Juan");
        Animal a = new Perro();
        a.comer();
        a.dormir();
        // a.moverCola();

        Perro p = (Perro) a; //top down casting
        p.moverCola();

        Animal perroConvertido = p; //bottom-up casting
        perroConvertido.dormir();

        Volador v = new Humano("Juan");
        v.volar();*/


        List<Animal> animales = new ArrayList<>();
        animales.add(new Humano("María"));
        animales.add(new Perro());
        animales.add(new Ave("Kikiriki"));
        animales.add(new Humano("Gustavo"));

        volar(animales);
        trabajar(animales);


    }

    private static void volar(List<Animal> animales) {
        for (Animal animal : animales) {
            if (animal instanceof Volador) {
                Volador v = (Volador) animal;
                v.volar();
            }
        }
    }

    private static void trabajar(List<Animal> animales) {
        for (Animal animal : animales) {
            if (animal instanceof Humano) {
                Humano h = (Humano) animal;
                h.trabajar();
            }
        }
    }


}
