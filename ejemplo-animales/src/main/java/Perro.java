public class Perro extends Animal{

    public Perro(String nombre) {
        super(nombre);
    }

    public Perro(){
        super("Perro desconocido");
    }

    public void moverCola(){
        System.out.println("Muevo la cola como perro");
    }

    @Override
    public void hacerRuido() {
        System.out.println("Guau!");
    }
}
