public abstract class Animal {
    //atributos
    protected String nombre;

    //constructor
    public Animal(String nombre){
        this.nombre = nombre;
    }

    //operaciones
    public void comer(){
        System.out.println("Como como animal");
    }

    public void dormir(){
        System.out.println("Duermo como animal");
    }

    public abstract void hacerRuido();

}
