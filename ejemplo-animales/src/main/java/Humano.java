public class Humano extends Animal implements Volador {
    private String id;

    public Humano(String nombre) {
        super(nombre);
    }

    public void trabajar(){
        System.out.printf("%s%s%s%n","Hola, soy: ", this.nombre, " y trabajo como animal");
    }

    @Override
    public void hacerRuido() {
        System.out.println("Bla bla bla!");
    }

    @Override
    public void volar() {
        System.out.println("Vuelo en avión");
    }
}
