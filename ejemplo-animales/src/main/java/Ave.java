public class Ave extends Animal implements Volador{
    public Ave(String nombre) {
        super(nombre);
    }

    @Override
    public void hacerRuido() {
        System.out.println("Cacarea");
    }

    @Override
    public void volar() {
        System.out.println("Vuelo como un ave");
    }
}
